package com.company;

public abstract class Car {

    private int moves;
    private final int ID;

    public Car(int moves, int id) {
        this.moves = moves;
        this.ID = id;
    }

    public void subtractOneMove() {
        moves--;
    }

    public int getId() {
        return ID;
    }

    public int getMoves() {
        return moves;
    }
}

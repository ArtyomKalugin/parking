package com.company;

import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;

public class ProcessSimulation {
    private static final String RED_COLOR = "\u001B[31m";
    private static final String GREEN_COLOR = "\u001B[32m";
    private static final String BLUE_COLOR = "\u001B[34m";
    private static final String RESET_COLOR = "\u001B[0m";
    private static final String PURPLE_COLOR = "\u001B[35m";
    private final Scanner SCANNER;
    private boolean isWorking;
    private final Random random;
    private Parking parking;
    private int carId;


    public ProcessSimulation() {
        random = new Random();
        isWorking = true;
        SCANNER = new Scanner(System.in);
        carId = 1000;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public void runMainCycle() {
        while (isWorking) {
            ArrayList<Car> cars = parking.getDefaultCars();
            ArrayList<Truck> trucks = parking.getTrucks();

            for (int i = 0; i < cars.size(); i++) {
                if (cars.get(i) != null) {
                    cars.get(i).subtractOneMove();
                    if (cars.get(i).getMoves() <= 0) {
                        if (cars.get(i) instanceof Truck) {
                            cars.set(i, null);
                            cars.set(i + 1, null);
                            i++;
                        } else {
                            cars.set(i, null);
                        }
                    }
                    if (cars.get(i) instanceof Truck) {
                        i++;
                    }
                }
            }

            for (int i = 0; i < trucks.size(); i++) {
                if (trucks.get(i) != null) {
                    trucks.get(i).subtractOneMove();
                    if (trucks.get(i).getMoves() <= 0) {
                        trucks.set(i, null);
                    }
                }
            }

            parking.setDefaultCars(cars);
            parking.setTrucks(trucks);


            int carsNow = random.nextInt((parking.getAllPlaces() / 3) + 1);

            for (int i = 0; i < carsNow; i++) {
                int move = random.nextInt(9) + 1;
                int carType = random.nextInt(2);

                if (carType == 1) {
                    int truckPlace = parking.showFreeTruckPlace();
                    Truck car = new Truck(move, carId);

                    if (truckPlace > -1) {
                        parking.setTrucks(truckPlace, car);
                    } else {
                        int[] takenPlacesOnDefaultParkingByTruck = parking.showFreePlacesForTrucksOnDefaultParking();
                        if (takenPlacesOnDefaultParkingByTruck[0] > -1 & takenPlacesOnDefaultParkingByTruck[1] > -1) {
                            if (takenPlacesOnDefaultParkingByTruck[1] - takenPlacesOnDefaultParkingByTruck[0] == 1) {
                                parking.setDefaultCars(takenPlacesOnDefaultParkingByTruck[0], car);
                                parking.setDefaultCars(takenPlacesOnDefaultParkingByTruck[1], car);
                            }
                        } else {
                            int shortestMove = countShortestMoves(cars);
                            System.out.println(RED_COLOR + "THERE IS NO FREE PLACE! WAIT " + shortestMove + " MOVES!" + RESET_COLOR);
                            break;
                        }
                    }

                } else {
                    int defaultPlace = parking.showFreeDefaultPlace();
                    PassengerCar car = new PassengerCar(move, carId);

                    if (defaultPlace > -1) {
                        parking.setDefaultCars(defaultPlace, car);
                    } else {
                        int shortestMove = countShortestMoves(cars);
                        System.out.println(RED_COLOR + "THERE IS NO FREE PLACE! WAIT " + shortestMove + " MOVES!" + RESET_COLOR);
                        break;
                    }
                }

                carId++;
            }

            System.out.println(BLUE_COLOR + "q - quit; n - next move; c - clear parking; i - show information" + RESET_COLOR);
            String message = SCANNER.nextLine();

            if (message.toLowerCase().equals("i")) {
                makeMessage(message);
                System.out.print(BLUE_COLOR + "WRITE ANOTHER INSTRUCTION: " + RESET_COLOR);
                message = SCANNER.nextLine();
                while (message.toLowerCase().equals("i")) {
                    makeMessage(message);
                    System.out.print(BLUE_COLOR + "WRITE ANOTHER INSTRUCTION: " + RESET_COLOR);
                    message = SCANNER.nextLine();
                }
            }

            while (!isAvailableMessage(message.toLowerCase())) {
                System.out.print(RED_COLOR + "I DON'T KNOW THIS INSTRUCTION! TRY AGAIN: " + RESET_COLOR);
                message = SCANNER.nextLine();
            }

            makeMessage(message.toLowerCase());

        }

    }

    private boolean isAvailableMessage(String message) {
        String[] availableMessages = new String[]{"n", "q", "c", "i"};

        return isInMassive(availableMessages, message);
    }

    private boolean isInMassive(String[] massive, String element) {
        for (String s : massive) {
            if (s.equals(element)) {
                return true;
            }
        }
        return false;
    }

    private void makeMessage(String message) {
        switch (message) {
            case "q" -> {
                System.out.println(RED_COLOR + "YOU HAVE FINISHED THIS SIMULATION" + RESET_COLOR);
                isWorking = false;
            }
            case "n" -> {
                System.out.println(BLUE_COLOR + "CONTINUE" + RESET_COLOR);
                isWorking = true;
            }
            case "c" -> {
                System.out.println(GREEN_COLOR + "ALL CARS WAS DELETED FROM PARKING" + RESET_COLOR);
                parking.resetParking();
            }
            case "i" -> {
                ArrayList<Car> cars = parking.getDefaultCars();
                ArrayList<Truck> trucks = parking.getTrucks();
                System.out.println(GREEN_COLOR + "THE SHORTEST MOVE: " + countShortestMoves(cars) + "\n" +
                        "FREE DEFAULT PLACES: " + countFreeDefaultPlaces(cars) + "\n" +
                        "TAKEN DEFAULT PLACES: " + countDefaultTakenPlaces(cars) + "\n" +
                        "FREE TRUCK PLACES: " + countFreeTruckPlaces(trucks) + "\n" +
                        "TAKEN TRUCK PLACES: " + countTruckTakenPlaces(trucks));

                System.out.println();
                System.out.println(BLUE_COLOR + "DEFAULT PARKING:" + RESET_COLOR);
                showDefaultParking(cars);

                System.out.println();
                System.out.println(BLUE_COLOR + "TRUCK PARKING:" + RESET_COLOR);
                showTruckParking(trucks);
            }
        }
    }

    private int countShortestMoves(ArrayList<Car> cars) {
        int shortestMove = 10;

        for (Car car : cars) {
            if (car != null) {
                if (car.getMoves() < shortestMove) {
                    shortestMove = car.getMoves();
                }
            }
        }

        return shortestMove;
    }

    private int countFreeDefaultPlaces(ArrayList<Car> cars) {
        int freePlaces = 0;
        for (Car car : cars) {
            if (car == null) {
                freePlaces++;
            }
        }
        return freePlaces;
    }

    private int countFreeTruckPlaces(ArrayList<Truck> cars){
        int freePlaces = 0;
        for (Car car : cars) {
            if (car == null) {
                freePlaces++;
            }
        }
        return freePlaces;
    }

    private int countTruckTakenPlaces(ArrayList<Truck> cars) {
        return parking.getTruckPlaces() - countFreeTruckPlaces(cars);

    }

    private int countDefaultTakenPlaces(ArrayList<Car> cars){
        return parking.getDefaultPlaces() - countFreeDefaultPlaces(cars);
    }

    private void showDefaultParking(ArrayList<Car> cars) {
        for (int i = 0; i < cars.size(); i++) {
            System.out.print(PURPLE_COLOR + "PLACE " + (i + 1) + ". " + RESET_COLOR);
            if (cars.get(i) == null) {
                System.out.print(PURPLE_COLOR + "IT'S FREE PLACE!" + RESET_COLOR + "\n");
            } else {
                if (cars.get(i) instanceof Truck) {
                    System.out.print(PURPLE_COLOR + "IT'S TRUCK! " + RESET_COLOR);
                }
                System.out.print(PURPLE_COLOR + "CAR ID: " + cars.get(i).getId() + ". MOVES LEFT: " +
                        cars.get(i).getMoves() + RESET_COLOR + "\n");
            }
        }
    }

    private void showTruckParking(ArrayList<Truck> cars) {
        for (int i = 0; i < cars.size(); i++) {
            System.out.print(PURPLE_COLOR + "PLACE " + (i + 1) + ". " + RESET_COLOR);
            if (cars.get(i) == null) {
                System.out.print(PURPLE_COLOR + "IT'S FREE PLACE!" + RESET_COLOR + "\n");
            } else {
                System.out.print(PURPLE_COLOR + "CAR ID: " + cars.get(i).getId() + ". MOVES LEFT: " +
                        cars.get(i).getMoves() + RESET_COLOR + "\n");
            }
        }
    }


}

package com.company;

import java.util.Scanner;

public class Moves {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Write the number of parking default places: ");
        int defaultPlacesOnParking = scanner.nextInt();
        while (defaultPlacesOnParking <= 0) {
            System.out.print("Write the correct number: ");
            defaultPlacesOnParking = scanner.nextInt();
        }

        System.out.print("Write the number of parking truck places: ");
        int truckPlacesOnParking = scanner.nextInt();
        while (truckPlacesOnParking <= 0) {
            System.out.print("Write the correct number: ");
            truckPlacesOnParking = scanner.nextInt();
        }

        Parking parking = new Parking(defaultPlacesOnParking, truckPlacesOnParking);
        ProcessSimulation simulation = new ProcessSimulation();
        simulation.setParking(parking);

        simulation.runMainCycle();
    }
}

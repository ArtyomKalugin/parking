package com.company;
import java.util.ArrayList;


public class Parking {
    private int defaultPlaces;
    private int truckPlaces;
    private ArrayList<Car> defaultCars;
    private ArrayList<Truck> trucks;

    public Parking(int defaultPlaces, int truckPlaces) {
        this.defaultPlaces = defaultPlaces;
        this.truckPlaces = truckPlaces;

        defaultCars = new ArrayList<>();
        fillDefaultParkingWithNulls();

        trucks = new ArrayList<>();
        fillTruckParkingWithNulls();
    }

    public int getDefaultPlaces() {
        return defaultPlaces;
    }

    private void fillDefaultParkingWithNulls(){
        for (int i = 0; i < defaultPlaces; i++) {
            defaultCars.add(null);
        }
    }

    private void fillTruckParkingWithNulls(){
        for (int i = 0; i < truckPlaces; i++) {
            trucks.add(null);
        }
    }

    public int getTruckPlaces() {
        return truckPlaces;
    }

    public int showFreeDefaultPlace() {
        for (int i = 0; i < defaultCars.size(); i++) {
            if (defaultCars.get(i) == null) {
                return i;
            }
        }
        return -1;
    }

    public int showFreeTruckPlace() {
        for (int i = 0; i < trucks.size(); i++) {
            if (trucks.get(i) == null) {
                return i;
            }
        }
        return -1;
    }

    public void setDefaultCars(int place, Car car) {
        defaultCars.set(place, car);
    }

    public void setDefaultCars(ArrayList<Car> defaultCars) {
        this.defaultCars = defaultCars;
    }

    public void setTrucks(ArrayList<Truck> trucks) {
        this.trucks = trucks;
    }

    public void setTrucks(int place, Truck truck) {
        trucks.set(place, truck);
    }

    public int getAllPlaces() {
        return truckPlaces + defaultPlaces;
    }

    public ArrayList<Car> getDefaultCars() {
        return defaultCars;
    }

    public ArrayList<Truck> getTrucks() {
        return trucks;
    }

    public void resetParking() {
        defaultCars = new ArrayList<>();
        fillDefaultParkingWithNulls();

        trucks = new ArrayList<>();
        fillTruckParkingWithNulls();
    }

    public int[] showFreePlacesForTrucksOnDefaultParking() {
        int firstPlace = -1;
        int secondPlace = -1;

        for (int i = 0; i < defaultCars.size(); i++) {
            if (defaultCars.get(i) == null & firstPlace == -1) {
                firstPlace = i;
            } else if (defaultCars.get(i) == null & firstPlace != -1 & secondPlace == -1) {
                secondPlace = i;
            }
        }
        return new int[]{firstPlace, secondPlace};
    }

    public void changeSizesOfParking(int size, String parkingName){
        if(parkingName.toLowerCase().equals("default parking")){
            defaultPlaces = size;
            defaultCars.ensureCapacity(defaultPlaces);
        }
        if(parkingName.toLowerCase().equals("truck parking")){
            truckPlaces = size;
            trucks.ensureCapacity(truckPlaces);
        }
    }
}
